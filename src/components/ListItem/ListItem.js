import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

const ListItem = (props) => (

    <TouchableOpacity onPress={props.onItemPressed}>
        <View style={styles.ListItem}>
            <Image resizeMode="cover" source={props.placeImage} style={styles.placeImage}/>
            <Text>{props.placeName}</Text>

        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    ListItem:{
        width: '100%',
        marginBottom:5,
        padding:10,
        backgroundColor:'#eee',
        flexDirection: 'row',
        alignItems: 'center',
    },
    placeImage: {
        marginRight: 8,
        height:50,
        width:50
    }
});
export default ListItem;